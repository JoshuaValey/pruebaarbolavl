﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomStructures.Structures;

namespace PruebaArbolAVL
{
    class Program
    {
        static void Main(string[] args)
        {
            //Prueba de hashMap
            HashMap<string, User> mapa = new HashMap<string, User>(11);

            User joshua = new User {
                Name = "Joshua",
                LastName = "Valey"
            };
            User herbert = new User {
                Name = "Herbert",
                LastName = "Horellana"
            };
            User sergio = new User {
                Name = "Sergio",
                LastName = "Crespin"
            };

            User joseline = new User
            {
                Name = "Joseline",
                LastName = "Huitz"
            };
            User oswaldo = new User
            {
                Name = "Oswaldo",
                LastName = "Hernandez"
            };
            User gabriel = new User
            {
                Name = "Gabriel",
                LastName = "Villatoro"
            };

            //-----
            User mario = new User
            {
                Name = "Mario",
                LastName = "Cuevas"
            };
            User hugo = new User
            {
                Name = "Hugo",
                LastName = "Soza"
            };
            User jorge = new User
            {
                Name = "Jorge",
                LastName = "Valey"
            };

            //-----
            User jose = new User
            {
                Name = "Jose",
                LastName = "de la Cerda"
            };
            User julio = new User
            {
                Name = "Julio",
                LastName = "Sandoval"
            };
            User ricardo = new User
            {
                Name = "Ricardo",
                LastName = "Salazar"
            };

            //-----
            User url = new User
            {
                Name = "URL",
                LastName = "Landivar"
            };
            User usuario1 = new User
            {
                Name = "Usuario1",
                LastName = "Usuario1"
            };
            User user2 = new User
            {
                Name = "user2",
                LastName = "user2"
            };


            mapa.Insert(joshua.Name, joshua);


            mapa.Insert(herbert.Name, herbert);

            mapa.Insert(sergio.Name, sergio);

            mapa.Insert(joseline.Name, joseline);

            mapa.Insert(oswaldo.Name, oswaldo);

            mapa.Insert(gabriel.Name, gabriel);
            
            mapa.Insert(mario.Name, mario);

            mapa.Insert(hugo.Name, hugo);

            mapa.Insert(jorge.Name, jorge);

            mapa.Insert(jose.Name, jose);

            mapa.Insert(julio.Name, julio);

            mapa.Insert(ricardo.Name, ricardo);

            mapa.Insert(url.Name, url);

            mapa.Insert(usuario1.Name, usuario1);

            mapa.Insert(user2.Name, user2);




            // mapa.Delete("Joshua");
            var dato = mapa.Search("Joshua");
            Console.WriteLine(dato.Name + " " + dato.LastName);

            /*
            var dato2 = mapa.Search("x");
            Console.WriteLine(dato2.Name);*/

           
            Console.ReadLine();
        }
    }
}


//Prueba de arbol avl
/* AvlTree<int> arbol = new AvlTree<int>();

           
            arbol.Insert(4);
            arbol.Insert(6);
            arbol.Insert(1);
            arbol.Insert(3);
            arbol.Insert(10);
            arbol.Insert(8);

            arbol.Insert(7);
            arbol.Insert(21);
            arbol.Insert(31);
            arbol.Insert(17);
            arbol.Insert(2);
            arbol.Insert(22);

            Console.WriteLine(arbol.TreeSize());
            Console.WriteLine(arbol.Height());
            int contador = 0;
            arbol.PrintTree(arbol.root, contador);*/





