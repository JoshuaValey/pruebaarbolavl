﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaArbolAVL
{
    public class User : IComparable
    {
        //key of hashTable
        public string Name { get; set; }
        public string LastName { get; set; }

        public int CompareTo(object obj)
        {
            return this.Name.CompareTo(obj.ToString());
        }
    }
}
