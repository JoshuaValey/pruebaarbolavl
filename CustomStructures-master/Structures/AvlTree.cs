﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomStructures.Interfaces;

namespace CustomStructures.Structures
{
   public class AvlTree<T> : INotLinearDataStructure<T> where T: IComparable
    {
        public NodeAvl<T> root = null;

        /// <summary>
        /// Mandar un contador inicial en 0
        /// </summary>
        /// <param name="actualRoot"></param>
        /// <param name="contador"></param>
        public void PrintTree(NodeAvl<T> actualRoot, int contador)
        {
            if (actualRoot == null)
            {
                return;
            }
            else
            {
                for (int i = 0; i < contador; i++)
                {
                    Console.Write("  ");
                }
                Console.WriteLine(actualRoot.Value + "\n");
                PrintTree(actualRoot.Left, contador+1);
            }
        }

        //Evaluete the priority of folowing code lines. 

        private int TreeNodeCount = 0;

        public int Height()
        {
            if (root == null)
                return 0;
            return root.Height;
        }

        public int TreeSize()
        {
            return TreeNodeCount;
        }

       

        /// <summary>
        /// Return true if the value exist in the tree. 
        /// </summary>
        /// <param name="value"></param>
        public bool Exist(T value)
        {
            return Exist(root, value);
        }
        private bool Exist(NodeAvl<T> actualRoot, T value)
        {
            if (actualRoot == null)
            {
                return false;
            }

            if (value.CompareTo(actualRoot.Value) == -1)
            {
                return Exist(actualRoot.Left, value);
            }
            else if (value.CompareTo(actualRoot.Value) == 1)
            {
                return Exist(actualRoot.Right, value);
            }
            else
            {
                return true;
            }
        }

        //Delete Functions
        public void Delete(T value)
        {
            if (Exist(root, value))
            {
                root = Delete(root, value);
                TreeNodeCount--;
            } 
        }

        private NodeAvl<T> Delete(NodeAvl<T> actualRoot, T value)
        {
            if (actualRoot == null)
                return null;

            //To left sub tree
            if (value.CompareTo(actualRoot.Value) == -1)
            {
                actualRoot.Left = Delete(actualRoot.Left, value);
            }

            //To Right sub tree
            else if (value.CompareTo(actualRoot.Value) == 1)
            {
                actualRoot.Right = Delete(actualRoot.Right, value);
            }
            //The node is found
            else
            {
                //just a right sub tree
                if (actualRoot.Left == null) return actualRoot.Right;
                //just a right sub tree 
                else if (actualRoot.Right == null) return actualRoot.Left;

                else
                {
                    //Remove from left sub tree.
                    if (actualRoot.Left.Height > actualRoot.Right.Height)
                    {
                        //find the biggest right node from the left. 
                        //sap values
                        T sonValue = MostRight(actualRoot.Left);
                        actualRoot.Value = sonValue;
                        //find the last node of the left sub tree
                        actualRoot.Left = Delete(actualRoot.Left, sonValue);
                    }

                    else
                    {
                        //find the smallest left node from the right
                        //swap values
                        T sonValue = MostLeft(actualRoot.Right);
                        actualRoot.Value = sonValue;

                        //int the right sub tree, remove the most left node.
                        actualRoot.Right = Delete(actualRoot.Right, sonValue);
                    }
                }
            }

            // Re balance the tree
            OverWriteBF(actualRoot);

            return Balance(actualRoot);

        }

        //find the most left node
        private T MostLeft(NodeAvl<T> node)
        {
            while (node.Left != null)
            {
                node = node.Left;
            }
            return node.Value;
        }
        //find the most right node
        private T MostRight(NodeAvl<T> node)
        {
            while (node.Right != null)
            {
                node = node.Right;
            }
            return node.Value; ;
        }
       
        //Insert Functions
        public void Insert(T value)
        {
            //If the value does not exist, then insert in the tree. 
            if (!Exist(root, value))
            {
                root = Insert(root, value);
                TreeNodeCount++;
            }
        }
        private NodeAvl<T> Insert(NodeAvl<T> actualRoot, T value)
        {
            if(actualRoot == null)
            {
                var node = new NodeAvl<T>();
                node.Value = value;
                return node;
            }

            if (value.CompareTo(actualRoot.Value) == -1)
            {
                actualRoot.Left = Insert(actualRoot.Left, value);
            }
            else
            {
                actualRoot.Right = Insert(actualRoot.Right, value);
            }

            //Add this lines to transform abb to avl tree

            //Re write balanceFactor and heigths
            OverWriteBF(actualRoot);

            return Balance(actualRoot);
        }

        // Search Functions
        public T Search(T value)
        {
            T result = Search(value, root).Value;

            return result;
        }
        private NodeAvl<T> Search(T value, NodeAvl<T> actualRoot)
        {
            if (actualRoot == null)
            {
                return null;
            }
            else if (value.CompareTo(actualRoot.Value) == 0)
            {
                return actualRoot;
            }
            else if (value.CompareTo(actualRoot.Value) == 1)
            {
                return Search(value, actualRoot.Right);
            }
            else
            {
                return Search(value, actualRoot.Left);
            }
        }


        #region Avl 

        /// <summary>
        /// This methot over write the balance factor and height of a node
        /// </summary>
        private void OverWriteBF(NodeAvl<T> node)
        {
            int leftHeight, rightHeight;

            if (node.Left == null) leftHeight = -1;
            else leftHeight = node.Left.Height;

            if (node.Right == null) rightHeight = -1;
            else rightHeight = node.Right.Height;


            node.Height = 1 + Math.Max(leftHeight, rightHeight);

            node.BalanceFactor = rightHeight - leftHeight;
    
        }

        /// <summary>
        /// Balance a node if its BF is > 2 && < -2
        /// </summary>
        /// <param name="node"></param>
        private NodeAvl<T> Balance(NodeAvl<T> node)
        {
            if (node.BalanceFactor == -2)
            {
                if (node.Left.BalanceFactor <= 0) return LeftToLeft(node);
                else return LeftToRight(node);
            }
            else if (node.BalanceFactor == 2)
            {
                if (node.Right.BalanceFactor >= 0) return RightToRight(node);
                else return RightToLeft(node);
            }

            return node;
        }
        private NodeAvl<T> LeftToLeft(NodeAvl<T> node)
        {
            return RotationToRight(node);
        }
        private NodeAvl<T> LeftToRight(NodeAvl<T> node)
        {
            node.Left = RotationToLeft(node.Left);
            return LeftToLeft(node);
        }
        private NodeAvl<T> RightToRight(NodeAvl<T> node)
        {
            return RotationToLeft(node);
        }
        private NodeAvl<T> RightToLeft(NodeAvl<T> node)
        {
            node.Right = RotationToRight(node.Right);
            return RightToRight(node);
        }
        private NodeAvl<T> RotationToLeft(NodeAvl<T> node)
        {
            NodeAvl<T> AuxParent = node.Right;
            node.Right = AuxParent.Left;
            AuxParent.Left = node;

            OverWriteBF(node);
            OverWriteBF(AuxParent);
            return AuxParent;
        }
        private NodeAvl<T> RotationToRight(NodeAvl<T> node)
        {
            NodeAvl<T> AuxParent = node.Left;
            node.Left = AuxParent.Right;
            AuxParent.Right = node;

            OverWriteBF(node);
            OverWriteBF(AuxParent);
            return AuxParent;
        }


        #endregion

       
    }
}
