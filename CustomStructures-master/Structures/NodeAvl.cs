﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomStructures.Structures
{
    public class NodeAvl<T>
    {
       

        public T Value { get; set; }
        public int BalanceFactor { get; set; }
        public int Height { get; set; }
        public NodeAvl<T> Left { get; set; }
        public NodeAvl<T> Right { get; set; }
    }
}
