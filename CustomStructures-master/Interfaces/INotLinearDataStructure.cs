﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomStructures.Interfaces
{
   interface INotLinearDataStructure<T> 
    {
        void Insert(T value);
        void Delete(T value);
        T Search(T value);

    }
}
